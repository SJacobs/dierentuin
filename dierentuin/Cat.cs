﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dierentuin
{
    public class Cat : Animal
    {
        public Cat(string name, Gender gender, string badHabits) : base(name, gender)
        {
            this.badHabits = badHabits;
        }
        public string badHabits { get; set; }

        public override string ToString()
        {
            return name + " is a  + " + gender + " + cat";
        }
    }
}
