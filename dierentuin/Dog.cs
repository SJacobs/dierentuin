﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dierentuin
{
    public class Dog : Animal
    {
        public DateTime lastWalk { get; set; }

        public Dog(string name, Gender gender, DateTime lastWalk) : base(name, gender)
        {
            this.lastWalk = lastWalk;
        }

        public override string ToString()
        {
            return name + " is a  + " + gender + " + dog";
        }
    }
}
