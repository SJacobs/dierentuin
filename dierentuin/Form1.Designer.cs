﻿namespace dierentuin
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listViewCat = new System.Windows.Forms.ListView();
            this.columnCatName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnCatGender = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ColumnBadHabits = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.buttonAddCat = new System.Windows.Forms.Button();
            this.tbName = new System.Windows.Forms.TextBox();
            this.lbName = new System.Windows.Forms.Label();
            this.lbGender = new System.Windows.Forms.Label();
            this.cbGender = new System.Windows.Forms.ComboBox();
            this.cbAnimal = new System.Windows.Forms.ComboBox();
            this.lbAnimal = new System.Windows.Forms.Label();
            this.tbBadHabits = new System.Windows.Forms.TextBox();
            this.dtpLastWalk = new System.Windows.Forms.DateTimePicker();
            this.lbBadHabits = new System.Windows.Forms.Label();
            this.gpCat = new System.Windows.Forms.GroupBox();
            this.lbLastWalk = new System.Windows.Forms.Label();
            this.gpAddAnimal = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.listViewDog = new System.Windows.Forms.ListView();
            this.columnDogName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnDogGender = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnLastWalk = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnReserve = new System.Windows.Forms.Button();
            this.gpReservation = new System.Windows.Forms.GroupBox();
            this.lbReserverName = new System.Windows.Forms.Label();
            this.tbReserverName = new System.Windows.Forms.TextBox();
            this.columnDogReserved = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnCatReserved = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.gpCat.SuspendLayout();
            this.gpAddAnimal.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.gpReservation.SuspendLayout();
            this.SuspendLayout();
            // 
            // listViewCat
            // 
            this.listViewCat.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnCatName,
            this.columnCatGender,
            this.ColumnBadHabits,
            this.columnCatReserved});
            this.listViewCat.FullRowSelect = true;
            this.listViewCat.GridLines = true;
            this.listViewCat.Location = new System.Drawing.Point(6, 19);
            this.listViewCat.Name = "listViewCat";
            this.listViewCat.Size = new System.Drawing.Size(358, 230);
            this.listViewCat.TabIndex = 0;
            this.listViewCat.UseCompatibleStateImageBehavior = false;
            this.listViewCat.View = System.Windows.Forms.View.Details;
            // 
            // columnCatName
            // 
            this.columnCatName.Text = "Name";
            this.columnCatName.Width = 107;
            // 
            // columnCatGender
            // 
            this.columnCatGender.Text = "Gender";
            this.columnCatGender.Width = 78;
            // 
            // ColumnBadHabits
            // 
            this.ColumnBadHabits.Text = "Bad habits";
            this.ColumnBadHabits.Width = 111;
            // 
            // buttonAddCat
            // 
            this.buttonAddCat.Location = new System.Drawing.Point(214, 219);
            this.buttonAddCat.Name = "buttonAddCat";
            this.buttonAddCat.Size = new System.Drawing.Size(75, 23);
            this.buttonAddCat.TabIndex = 1;
            this.buttonAddCat.Text = "Add";
            this.buttonAddCat.UseVisualStyleBackColor = true;
            this.buttonAddCat.Click += new System.EventHandler(this.buttonAddCat_Click);
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(77, 31);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(200, 20);
            this.tbName.TabIndex = 3;
            // 
            // lbName
            // 
            this.lbName.AutoSize = true;
            this.lbName.Location = new System.Drawing.Point(36, 31);
            this.lbName.Name = "lbName";
            this.lbName.Size = new System.Drawing.Size(35, 13);
            this.lbName.TabIndex = 5;
            this.lbName.Text = "Name";
            // 
            // lbGender
            // 
            this.lbGender.AutoSize = true;
            this.lbGender.Location = new System.Drawing.Point(29, 60);
            this.lbGender.Name = "lbGender";
            this.lbGender.Size = new System.Drawing.Size(42, 13);
            this.lbGender.TabIndex = 6;
            this.lbGender.Text = "Gender";
            // 
            // cbGender
            // 
            this.cbGender.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbGender.FormattingEnabled = true;
            this.cbGender.Items.AddRange(new object[] {
            "Male",
            "Female"});
            this.cbGender.Location = new System.Drawing.Point(77, 57);
            this.cbGender.Name = "cbGender";
            this.cbGender.Size = new System.Drawing.Size(200, 21);
            this.cbGender.TabIndex = 7;
            // 
            // cbAnimal
            // 
            this.cbAnimal.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbAnimal.FormattingEnabled = true;
            this.cbAnimal.Items.AddRange(new object[] {
            "Cat",
            "Dog"});
            this.cbAnimal.Location = new System.Drawing.Point(77, 84);
            this.cbAnimal.Name = "cbAnimal";
            this.cbAnimal.Size = new System.Drawing.Size(200, 21);
            this.cbAnimal.TabIndex = 9;
            this.cbAnimal.SelectedIndexChanged += new System.EventHandler(this.cbAnimal_SelectedIndexChanged);
            // 
            // lbAnimal
            // 
            this.lbAnimal.AutoSize = true;
            this.lbAnimal.Location = new System.Drawing.Point(33, 87);
            this.lbAnimal.Name = "lbAnimal";
            this.lbAnimal.Size = new System.Drawing.Size(38, 13);
            this.lbAnimal.TabIndex = 8;
            this.lbAnimal.Text = "Animal";
            // 
            // tbBadHabits
            // 
            this.tbBadHabits.Location = new System.Drawing.Point(77, 111);
            this.tbBadHabits.Name = "tbBadHabits";
            this.tbBadHabits.Size = new System.Drawing.Size(200, 20);
            this.tbBadHabits.TabIndex = 10;
            // 
            // dtpLastWalk
            // 
            this.dtpLastWalk.Location = new System.Drawing.Point(77, 111);
            this.dtpLastWalk.Name = "dtpLastWalk";
            this.dtpLastWalk.Size = new System.Drawing.Size(200, 20);
            this.dtpLastWalk.TabIndex = 11;
            // 
            // lbBadHabits
            // 
            this.lbBadHabits.AutoSize = true;
            this.lbBadHabits.Location = new System.Drawing.Point(14, 114);
            this.lbBadHabits.Name = "lbBadHabits";
            this.lbBadHabits.Size = new System.Drawing.Size(57, 13);
            this.lbBadHabits.TabIndex = 12;
            this.lbBadHabits.Text = "Bad habits";
            // 
            // gpCat
            // 
            this.gpCat.Controls.Add(this.listViewCat);
            this.gpCat.Location = new System.Drawing.Point(12, 12);
            this.gpCat.Name = "gpCat";
            this.gpCat.Size = new System.Drawing.Size(370, 255);
            this.gpCat.TabIndex = 13;
            this.gpCat.TabStop = false;
            this.gpCat.Text = "Cats";
            // 
            // lbLastWalk
            // 
            this.lbLastWalk.AutoSize = true;
            this.lbLastWalk.Location = new System.Drawing.Point(19, 114);
            this.lbLastWalk.Name = "lbLastWalk";
            this.lbLastWalk.Size = new System.Drawing.Size(52, 13);
            this.lbLastWalk.TabIndex = 14;
            this.lbLastWalk.Text = "Last walk";
            // 
            // gpAddAnimal
            // 
            this.gpAddAnimal.Controls.Add(this.lbName);
            this.gpAddAnimal.Controls.Add(this.lbLastWalk);
            this.gpAddAnimal.Controls.Add(this.buttonAddCat);
            this.gpAddAnimal.Controls.Add(this.tbName);
            this.gpAddAnimal.Controls.Add(this.lbBadHabits);
            this.gpAddAnimal.Controls.Add(this.lbGender);
            this.gpAddAnimal.Controls.Add(this.tbBadHabits);
            this.gpAddAnimal.Controls.Add(this.cbGender);
            this.gpAddAnimal.Controls.Add(this.dtpLastWalk);
            this.gpAddAnimal.Controls.Add(this.lbAnimal);
            this.gpAddAnimal.Controls.Add(this.cbAnimal);
            this.gpAddAnimal.Location = new System.Drawing.Point(388, 12);
            this.gpAddAnimal.Name = "gpAddAnimal";
            this.gpAddAnimal.Size = new System.Drawing.Size(295, 255);
            this.gpAddAnimal.TabIndex = 15;
            this.gpAddAnimal.TabStop = false;
            this.gpAddAnimal.Text = "Add animal";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.listViewDog);
            this.groupBox1.Location = new System.Drawing.Point(12, 273);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(370, 255);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Dog";
            // 
            // listViewDog
            // 
            this.listViewDog.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnDogName,
            this.columnDogGender,
            this.columnLastWalk,
            this.columnDogReserved});
            this.listViewDog.FullRowSelect = true;
            this.listViewDog.GridLines = true;
            this.listViewDog.Location = new System.Drawing.Point(6, 19);
            this.listViewDog.Name = "listViewDog";
            this.listViewDog.Size = new System.Drawing.Size(358, 230);
            this.listViewDog.TabIndex = 0;
            this.listViewDog.UseCompatibleStateImageBehavior = false;
            this.listViewDog.View = System.Windows.Forms.View.Details;
            // 
            // columnDogName
            // 
            this.columnDogName.Text = "Name";
            this.columnDogName.Width = 107;
            // 
            // columnDogGender
            // 
            this.columnDogGender.Text = "Gender";
            this.columnDogGender.Width = 70;
            // 
            // columnLastWalk
            // 
            this.columnLastWalk.Text = "Last walk";
            this.columnLastWalk.Width = 116;
            // 
            // btnReserve
            // 
            this.btnReserve.Location = new System.Drawing.Point(214, 226);
            this.btnReserve.Name = "btnReserve";
            this.btnReserve.Size = new System.Drawing.Size(75, 23);
            this.btnReserve.TabIndex = 2;
            this.btnReserve.Text = "Reserve";
            this.btnReserve.UseVisualStyleBackColor = true;
            this.btnReserve.Click += new System.EventHandler(this.btnReserve_Click);
            // 
            // gpReservation
            // 
            this.gpReservation.Controls.Add(this.tbReserverName);
            this.gpReservation.Controls.Add(this.lbReserverName);
            this.gpReservation.Controls.Add(this.btnReserve);
            this.gpReservation.Location = new System.Drawing.Point(388, 273);
            this.gpReservation.Name = "gpReservation";
            this.gpReservation.Size = new System.Drawing.Size(295, 255);
            this.gpReservation.TabIndex = 16;
            this.gpReservation.TabStop = false;
            this.gpReservation.Text = "Reserve";
            // 
            // lbReserverName
            // 
            this.lbReserverName.AutoSize = true;
            this.lbReserverName.Location = new System.Drawing.Point(36, 31);
            this.lbReserverName.Name = "lbReserverName";
            this.lbReserverName.Size = new System.Drawing.Size(35, 13);
            this.lbReserverName.TabIndex = 3;
            this.lbReserverName.Text = "Name";
            // 
            // tbReserverName
            // 
            this.tbReserverName.Location = new System.Drawing.Point(77, 28);
            this.tbReserverName.Name = "tbReserverName";
            this.tbReserverName.Size = new System.Drawing.Size(200, 20);
            this.tbReserverName.TabIndex = 15;
            // 
            // columnDogReserved
            // 
            this.columnDogReserved.Text = "Reserved";
            // 
            // columnCatReserved
            // 
            this.columnCatReserved.Text = "Reserved";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(694, 537);
            this.Controls.Add(this.gpReservation);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.gpAddAnimal);
            this.Controls.Add(this.gpCat);
            this.Name = "Form1";
            this.Text = "Animal shelter";
            this.gpCat.ResumeLayout(false);
            this.gpAddAnimal.ResumeLayout(false);
            this.gpAddAnimal.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.gpReservation.ResumeLayout(false);
            this.gpReservation.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView listViewCat;
        private System.Windows.Forms.ColumnHeader columnCatName;
        private System.Windows.Forms.ColumnHeader columnCatGender;
        private System.Windows.Forms.ColumnHeader ColumnBadHabits;
        private System.Windows.Forms.Button buttonAddCat;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.Label lbName;
        private System.Windows.Forms.Label lbGender;
        private System.Windows.Forms.ComboBox cbGender;
        private System.Windows.Forms.ComboBox cbAnimal;
        private System.Windows.Forms.Label lbAnimal;
        private System.Windows.Forms.TextBox tbBadHabits;
        private System.Windows.Forms.DateTimePicker dtpLastWalk;
        private System.Windows.Forms.Label lbBadHabits;
        private System.Windows.Forms.GroupBox gpCat;
        private System.Windows.Forms.Label lbLastWalk;
        private System.Windows.Forms.GroupBox gpAddAnimal;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ListView listViewDog;
        private System.Windows.Forms.ColumnHeader columnDogName;
        private System.Windows.Forms.ColumnHeader columnDogGender;
        private System.Windows.Forms.ColumnHeader columnLastWalk;
        private System.Windows.Forms.Button btnReserve;
        private System.Windows.Forms.GroupBox gpReservation;
        private System.Windows.Forms.ColumnHeader columnCatReserved;
        private System.Windows.Forms.ColumnHeader columnDogReserved;
        private System.Windows.Forms.TextBox tbReserverName;
        private System.Windows.Forms.Label lbReserverName;
    }
}

