﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace dierentuin
{
    public abstract class Animal : ISellable
    {
        public string name { get; set; }
        public Gender gender { get; set; }
        public bool reserved { get; set; }

        public string Name
        {
            get
            {
                return name;
            }
        }

        public decimal Price
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public Animal(string name, Gender gender)
        {
            this.name = name;
            this.gender = gender;
            reserved = false;
        }
    }
}
