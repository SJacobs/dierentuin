﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace dierentuin
{
    public partial class Form1 : Form
    {
        private Reservation reservation;

        public Form1()
        {
            InitializeComponent();
            reservation = new Reservation();
            prepareForm();
        }

        private void prepareForm()
        {
            cbGender.SelectedIndex = 0;
            switchToCat();
        }

        private void switchToCat()
        {
            cbAnimal.SelectedIndex = 0;
            dtpLastWalk.Hide();
            lbLastWalk.Hide();
        }

        private void switchToDog()
        {
            cbAnimal.SelectedIndex = 1;
            tbBadHabits.Hide();
            lbBadHabits.Hide();
            dtpLastWalk.Show();
            lbLastWalk.Show();
        }

        public void addAnimal()
        {
            string name = tbName.Text;
            Gender gender = (Gender)Enum.Parse(typeof(Gender), cbGender.Text.ToUpper());

            ListViewItem listViewItem = new ListViewItem(tbName.Text);
            listViewItem.SubItems.Add(cbGender.Text);

            if (cbAnimal.Text == "Cat")
            {
                string badHabits = tbBadHabits.Text;
                Cat cat = new Cat(name, gender, badHabits);
                reservation.addCat(cat, listViewCat);
            }
            else if (cbAnimal.Text == "Dog")
            {
                DateTime reservedAt = DateTime.Parse(dtpLastWalk.Text);
                DateTime lastWalk = DateTime.Parse(dtpLastWalk.Text);
                Dog dog = new Dog(name, gender, lastWalk);
                // listViewItem.SubItems.Add(dog.lastWalk.ToString());
                //  listViewItem.SubItems.Add(dog.reserved.ToString());
                reservation.addDog(dog, listViewDog);
            }
        }

        private void buttonAddCat_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(tbName.Text))
            {
                MessageBox.Show("Name is required.");
                return;
            }

            addAnimal();
            tbName.Clear();
            tbBadHabits.Clear();
        }

        private void cbAnimal_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbAnimal.Text == "Cat")
            {
                switchToCat();
            }
            if (cbAnimal.Text == "Dog")
            {
                switchToDog();
            }
        }

        private void btnReserve_Click(object sender, EventArgs e)
        {
            foreach (int id in listViewCat.SelectedIndices)
            {
                if (reservation.reserve(tbReserverName.Text, reservation.cats[id]))
                    listViewCat.Items[id].SubItems[listViewCat.Items[id].SubItems.Count - 1].Text = "Yes";
                else
                    MessageBox.Show(reservation.cats[id].name + " has already been reserved.");
            }

            foreach (int id in listViewDog.SelectedIndices)
            {
                if (reservation.reserve(tbReserverName.Text, reservation.dogs[id]))
                    listViewDog.Items[id].SubItems[listViewDog.Items[id].SubItems.Count - 1].Text = "Yes";
                else
                    MessageBox.Show(reservation.dogs[id].name + " has already been reserved.");
            }
        }
    }
}
