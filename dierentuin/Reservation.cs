﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace dierentuin
{
    public class Reservation
    {
        public List<Cat> cats { get; set; }
        public List<Dog> dogs { get; set; }
        public List<Reservor> reservors { get; set; }

        public Reservation()
        {
            cats = new List<Cat>();
            dogs = new List<Dog>();
            reservors = new List<Reservor>();
        }

        public void addCat(Cat cat, ListView listView)
        {
            cats.Add(cat);

            ListViewItem listViewItem = new ListViewItem(cat.name);
            string gender = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(cat.gender.ToString().ToLower());
            listViewItem.SubItems.Add(gender);
            listViewItem.SubItems.Add(cat.badHabits);
            listViewItem.SubItems.Add(cat.reserved ? "Yes" : "No");
            listView.Items.Add(listViewItem);
        }

        public void addDog(Dog dog, ListView listView)
        {
            dogs.Add(dog);

            ListViewItem listViewItem = new ListViewItem(dog.name);
            string gender = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(dog.gender.ToString().ToLower());
            listViewItem.SubItems.Add(gender);
            listViewItem.SubItems.Add(dog.lastWalk.ToString());
            listViewItem.SubItems.Add(dog.reserved ? "Yes" : "No");
            listView.Items.Add(listViewItem);
        }

        public bool reserve(string name, Animal animal)
        {
            if (animal.reserved)
                return false;

            animal.reserved = true;
            Reservor r = new Reservor(name, animal, DateTime.Now);
            reservors.Add(r);
            return true;
        }
    }
}
