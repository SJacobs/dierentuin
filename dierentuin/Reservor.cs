﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dierentuin
{
    public class Reservor
    {
        public string name { get; set; }
        public Animal animal { get; set; }
        public DateTime reservedAt { get; set; }

        public Reservor(string name, Animal animal, DateTime reservedAt)
        {
            this.name = name;
            this.animal = animal;
            this.reservedAt = reservedAt;
        }
    }
}
