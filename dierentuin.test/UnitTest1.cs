﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace dierentuin.test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void CreateCat()
        {
            Animal animal = new Cat("Lola", Gender.FEMALE, "Some habit");
            Assert.AreEqual(animal.name, "Lola");
            Assert.AreEqual(animal.gender, Gender.FEMALE);
            Assert.AreEqual(((Cat)animal).badHabits, "Some habit");
            Assert.AreEqual(animal.reserved, false);
        }

        [TestMethod]
        public void CreateDog()
        {
            DateTime now = DateTime.Now;
            Animal animal = new Dog("Tom", Gender.MALE, now);
            Assert.AreEqual(animal.name, "Tom");
            Assert.AreEqual(animal.gender, Gender.MALE);
            Assert.AreEqual(((Dog)animal).lastWalk, now);
            Assert.AreEqual(animal.reserved, false);
        }
    }
}
